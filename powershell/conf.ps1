$regexDB = "(Data Source)=([^\s]+;)"
$regexUserId = "(User ID)=([^\s]+;)"
$regexInitialCatalog = "(Initial Catalog)=([^\s]+;)"

function updateConnectionString {
    param ([string] $path, [string] $initialCatalog, [string] $hostdb, [string] $userdb, [string] $passdb)
    [Xml]$xml = Get-Content $path
    $connString = $xml.connectionStrings.add.connectionString
    $connString = $connString -replace $regexDB, -join('Data Source=', $hostdb, ';')
    $connString = $connString -replace $regexInitialCatalog, -join('Initial Catalog=', $initialCatalog, ';')
    $connString = $connString -replace $regexUserId, -join('User ID=', $userdb, ';', 'Password=', $passdb, ';')
    $xml.connectionStrings.add.connectionString = $connString
    $xml.Save($path)
}

function updateAppSettings {
    param ([string] $path, [string] $value)
    [Xml]$xml = Get-Content $path
    $add = $xml.appSettings.add | Where-Object { $_.key -eq "Environment"}
    $add.value = $value
    $xml.Save($path)
}

function updateConnectionAppConfig {
    param ([string] $path, [string] $initialCatalog, [string] $hostdb, [string] $userdb, [string] $passdb)
    [Xml]$xml = Get-Content $path
    $connString = $xml.configuration.connectionStrings.add.connectionString
    $connString = $connString -replace $regexDB, -join('Data Source=', $hostdb, ';')
    $connString = $connString -replace $regexInitialCatalog, -join('Initial Catalog=', $initialCatalog, ';')
    $connString = $connString -replace $regexUserId, -join('User ID=', $userdb, ';', 'Password=', $passdb, ';')
    $xml.configuration.connectionStrings.add.connectionString = $connString
    $xml.Save($path)
}

function updateIntegrationLog4NetConfig {
    param ([string] $path, [string] $initialCatalog, [string] $hostdb, [string] $userdb, [string] $passdb)
    [Xml]$xml = Get-Content $path
    $connString = $xml.configuration.log4net.appender | Where-Object { $_.name -eq "AdoNetAppender"}
    $urlConnString = $connString.connectionString.value -replace $regexDB, -join('Data Source=', $hostdb, ';')
    $urlConnString = $connString -replace $regexInitialCatalog, -join('Initial Catalog=', $initialCatalog, ';')
    $urlConnString = $urlConnString -replace $regexUserId, -join('User ID=', $userdb, ';', 'Password=', $passdb, ';')
    $connString.connectionString.value = $urlConnString
    $xml.Save($path)
}