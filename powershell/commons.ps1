function createFolder {
    param ([string] $path)
    New-Item -Path $path -ItemType Directory
}

function removeFolder {
    param ([string] $folderPath)
    Remove-Item -Path $folderPath -Recurse
}

function folderExists {
    param ([string] $path)
    return Test-Path -Path $path
}

<#
    Before create folder, the script check:
    * If folder exists and delete option seted true, folder is removed and recreated.
    * If folder exists and delete option seted false, nothing happens.
#>
function createFolderCheckingIfExists {
    param ([string]$folder,
           [bool]$deleteIfExists = $false)
    if (folderExists $folder) {
        if ($deleteIfExists) {
            removeFolder $folder
            createFolder $folder
        }
    } else {
        createFolder $folder
    }
}

<#
    Change .NET Framework version inside entire project.
#>
function changeTargetFrameworkVersion {
    param ([string]$newVersion,
           [string]$path)
    $regexAppConfig = '\ssku=".NETFramework,Version=v(.*)?"\s*(>*)'
    $regexPackage = 'targetFramework="net(.*)?"\s*(>*)'
    $regexCsproj = '(<TargetFrameworkVersion>v.*<)'
    $regexPackageGroup = '(<TargetFramework>net.*<)'
    Get-ChildItem $path -Recurse -Filter *.csproj | ForEach {
        $content = Get-Content $_.FullName
        $content | ForEach {
            $newValue = '<TargetFrameworkVersion>v' + $newVersion + '<'
            $_ -replace $regexCsproj, $newValue
        } | Set-Content $_.FullName
    }

    Get-ChildItem $path -Recurse -Filter *.csproj | ForEach {
        $content = Get-Content $_.FullName
        $content | ForEach {
            $newValue = '<TargetFramework>net' + $newVersion.Replace('.', '') + '<'
            $_ -replace $regexPackageGroup, $newValue
        } | Set-Content $_.FullName
    }

    Get-ChildItem $path -Recurse -Filter app.config | ForEach {
        $content = Get-Content $_.FullName
        $content | ForEach {
            $newValue = ' sku=".NETFramework,Version=v' + $newVersion + '"'
            $_ -replace $regexAppConfig, $newValue
        } | Set-Content $_.FullName
    }

    Get-ChildItem $path -Recurse -Filter packages.config | ForEach {
        $content = Get-Content $_.FullName
        $content | ForEach {
            $newValue = 'targetFramework="net' + $newVersion.Replace('.', '') + '"'
            $_ -replace $regexPackage, $newValue
        } | Set-Content $_.FullName
    }
}

function changeValueByRegexIntoFile {
    param ([string]$file,
           [string]$regex,
           [string]$value)
    
    Get-ChildItem $file | ForEach {
        $content = Get-Content $_.FullName
        $content | ForEach {
            $_ -replace $regex, $value
        } | Set-Content $_.FullName
    }
}