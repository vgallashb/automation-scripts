<#
.SYNOPSIS  
    Script to run sonarqube analysis.
.DESCRIPTION  
    Script to run sonarqube analysis.
    This script must be executed inside automation-scripts directory.
.NOTES  
    File Name  : sonar.ps1  
    Author     : Vinicius Gallas
.EXAMPLE  
    .\sonar.ps1 -projectName 'Project-key' -url 'http://localhost:9000' 
    -login 'fjdkajfkdjasfkdjask' -pathXMLCoverage '.\coverage\coverage.xml' -action 'begin'
.PARAMETER projectName  
    Project key.
.PARAMETER url  
    Sonarqube URL.
.PARAMETER login  
    User login token.
.PARAMETER pathProject  
    Project directory path.
.PARAMETER pathXMLCoverage  
    Path of XML coverage generated in test step.
.PARAMETER coverageExclusions  
    Files to exclusion in coverage analysis.
.PARAMETER action  
    Action can be 'begin' or 'end'.
#>
param([string]$projectName = 'project',
    [string]$url = '',
    [string]$login = '',
    [string]$pathProject = '',
    [string]$pathXMLCoverage = '',
    [string]$coverageExclusions = '',
    [string]$action = 'begin')

$currentLocation = Get-Location;
$msbuildScannerPath = "$currentLocation\tools\sonar-scanner\msbuild\SonarScanner.MSBuild.exe";

function startScanner {
    $command = "`& `"$msbuildScannerPath`" begin $(Get-BuildArgs 'begin')"
    Invoke-Expression $command
}

function endScanner {
    $command = "`& `"$msbuildScannerPath`" end $(Get-BuildArgs 'end')"
    Invoke-Expression $command
}

function Get-BuildArgs {
    if ($action.ToLower() -eq 'begin') {
        $argsSonar = '';
        $argsSonar = "/k:`"" + $projectName + "`" "
        $argsSonar = $argsSonar + "/d:sonar.host.url=`"" + $url + "`" "
        $argsSonar = $argsSonar + "/d:sonar.login=`"" + $login + "`" "
        $argsSonar = $argsSonar + "/d:sonar.cs.opencover.reportsPaths=`"" + $pathXMLCoverage + "`" "
        if ($coverageExclusions -ne '') {
            $argsSonar = $argsSonar + "/d:sonar.coverage.exclusions=`"" + $coverageExclusions + "`" "
        }
    } else {
        $argsSonar = $argsSonar + "/d:sonar.login=`"" + $login + "`" "
    }

    return $argsSonar
}

Set-Location $pathProject
if ($action.ToLower() -eq 'begin') {
    startScanner
} else {
    endScanner
}
Set-Location $currentLocation