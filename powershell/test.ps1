<#
.SYNOPSIS  
    Script to run .NET tests with VSTEST.
.DESCRIPTION  
    Script to run .NET tests with VSTEST and Opencover tool to generate code coverage as well.
    This script must be executed inside automation-scripts directory.
.NOTES  
    File Name  : test.ps1  
    Author     : Vinicius Gallas
.EXAMPLE  
    test.ps1 -vstestPath 'C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\Common7\IDE\CommonExtensions\Microsoft\TestWindow\vstest.console.exe' 
    -testFiles '"C:\TestsProjects\bin\release\tests.dll" "C:\TestsProjects\bin\release\another_tests.dll"' 
    -filterOpencover '"+[ProjectName*]* -[*Tests*]*"' -directoryProject 'Directory\path' -generateReport 'N'
.PARAMETER vstestPath  
    VSTEST.exe full path.
.PARAMETER testFiles  
    Path of .dll file tests. When there is more then one file, separe them with space.
.PARAMETER filterOpencover  
    Inclusion and exclusion filter used by opencover. Reference: https://github.com/opencover/opencover/wiki/Usage.
.PARAMETER directoryProject  
    Path of directory project.
.PARAMETER generateReport  
    Type 'S' or 'N' to generate HTML coverage report. By default, is not 'N'.
#>
param([string]$vstestPath = 'C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\Common7\IDE\CommonExtensions\Microsoft\TestWindow\vstest.console.exe',
    [string]$testFiles = '',
    [string]$filterOpencover = '+[*]*',
    [string]$directoryProject = '',
    [string]$generateReport = 'N')

. '.\powershell\commons.ps1'

$pathXMLCoverage = $directoryProject + '\coverage';
$pathHTMLReportCoverage = $pathXMLCoverage + '\report';

<#
    Find all .dll files that name end with Tests.dll inside project directory passed by argument.
#>
function Get-SearchTestsAssemblies {
    $testFiles = ''
    Get-ChildItem $directoryProject -Recurse -Filter 'Release' |
    ForEach-Object {
        if ($_.Parent.Name -eq 'bin') {
            $folder = $_
            Get-ChildItem $folder.FullName -Filter '*Tests.dll' |
            ForEach-Object {
                $testFiles = $testFiles + "`"" + $_.FullName + "`" "
            }
        }
    }

    return $testFiles
}

function runOpenCover {
    param ([string] $testFiles)

    & '.\tools\opencover\OpenCover.Console.exe' `
    -target:$vstestPath `
    -targetargs:$testFiles `
    -output:"$pathXMLCoverage\coverage.xml" `
    -filter:$filterOpencover `
    -register:user
}

function runVsTest {
    $command = "`& `"$vstestPath`" $(Get-SearchTestsAssemblies) /logger:trx /InIsolation /ResultsDirectory:'$pathXMLCoverage'"
    Invoke-Expression $command
    Get-ChildItem -Path $pathXMLCoverage *.trx -File | Rename-Item -NewName { $_.Name -Replace $_.Name, 'test.xml' }
}

function transformTest {
    & '.\tools\transform\Transform.exe' `
    -s: "$pathXMLCoverage\.test.xml" `
    -xsl:'.\tools\xslt\trxToSonarQubeGenericTestData.xsl' `
    -o: "$pathXMLCoverage\test.transformed.xml"
}

function runReportGenerator {
    & '.\tools\report-generator\ReportGenerator.exe' `
    -reports:"$pathXMLCoverage\coverage.xml" `
    -targetdir:$pathHTMLReportCoverage
    -reportTypes:htmlInline
}

createFolderCheckingIfExists -folder $pathXMLCoverage -deleteIfExists $true
createFolderCheckingIfExists -folder $pathHTMLReportCoverage -deleteIfExists $true

runOpenCover $testFiles

if ($generateReport -eq 'S') {
    runReportGenerator
}